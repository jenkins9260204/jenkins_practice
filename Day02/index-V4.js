const http = require('http');
const exported = require('./sunbeam');

const employees = [
                new exported.Emp(11, "mahesh1", "pune1"),
                new exported.Emp(12, "mahesh2", "pune2"),
                new exported.Emp(13, "mahesh3", "pune3"),
                new exported.Emp(14, "mahesh4", "pune4")
                ]

const server =  http.createServer((request, response)=>
{
  //  response.write("U requested " +  request.url );
    // response.setHeader('Content-Type', 'text/html')
    // response.write(`<h1>Welcome to Node Server</h1>`);
    if(request.url =="/employees" && 
            request.method.toLowerCase()=="get")
    {
        response.setHeader('Content-Type', 
                            'application/json');
        response.write(JSON.stringify(employees));
    }
    else if(request.url =="/employees" && 
            request.method.toLowerCase()=="post")
    {
        response.write("U request /employees via POST" );
    }
    else if(request.url =="/employees" && 
            request.method.toLowerCase()=="put")
        {
        response.write("U request /employees via PUT" );
        }
    else if(request.url =="/employees" && 
             request.method.toLowerCase()=="delete")
    {
    response.write("U request /employees via DELETE" );
    }
    
    response.end();
});

server.listen(9999,()=>{
                            console.log("Server Started on Port No 9999");
                        })