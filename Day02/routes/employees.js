const express = require('express')
const app = express();
const mysql = require('mysql');

var connection =  mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'manager',
    database : 'punedb'
});

app.use(express.json());

connection.connect();
app.get("/", (req, res)=>
{
    //connection.connect();
    connection.query("select * from Emp", (error, result)=>
    {
      if(error!=null)
       {
        res.send(error);
        //connection.end();
       }
       else
       {
        res.send(result);
        //connection.end();
       }
    })
   
});

app.post("/", (req, res)=>
{
    // console.log(req.body);
    // //Insert code here
    // res.send("POST Received for /Employees");

    ////connection.connect();
    let query = `insert into Emp values(${req.body.No}, 
                                       '${req.body.Name}', 
                                       '${req.body.Address}');`
    connection.query(query, (error, result)=>
    {
      if(error!=null)
       {
        res.send(error);
        ////connection.end();
       }
       else
       {
        res.send(result);
        //connection.end();
       }
    })
});

app.put("/:no", (req, res)=>{
    // console.log(req.body);
    // console.log(req.params.no)
    // //Update code here
    // res.send("PUT Received for /Employees");

    //connection.connect();
    let query = `update Emp set Name = '${req.body.Name}',  
                                Address = '${req.body.Address}' 
                                where No = ${req.params.no}`;
    connection.query(query, (error, result)=>
    {
      if(error!=null)
       {
        res.send(error);
        //connection.end();
       }
       else
       {
        res.send(result);
        //connection.end();
       }
    })
});

app.delete("/:no", (req, res)=>{
    // console.log(req.params.no)
    // //Delete code here
    // res.send("DELETE Received for /Employees");

    
   // connection.connect();
    let query = `delete from Emp where No = ${req.params.no}`;
    connection.query(query, (error, result)=>
    {
      if(error!=null)
       {
        res.send(error);
        //connection.end();
       }
       else
       {
        res.send(result);
        //connection.end();
       }
    })
});


module.exports = app;