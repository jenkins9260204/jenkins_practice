function GoodMorning()
{
    return "Good Morning";
}

function GoodBye()
{
    return "Good Bye";
}

class Emp
{
    constructor(no, name, address)
    {
        this.No = no;
        this.Name = name;
        this.Address = address;
    }

    GetDetails = ()=>{
        return this.No + this.Name + this.Address;
    }
}

var interestRate = 18;



module.exports = {
                    GoodMorning , 
                    GoodBye, 
                    Emp, 
                    interestRate
                };
