const express = require('express');
const employeesApp =  require('./routes/employees');
const adminApp =  require('./routes/admin');

const app = express();
app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin',"*");
    //line above  will only make GET possible in case of CORS issue

    res.setHeader('Access-Control-Allow-Methods',"*");
    //line abovewill make GET/POST/PUT/DELETE possible in case of CORS 
    
    res.setHeader('Access-Control-Allow-Headers',"*");
    //line abovewill make getting authorization etc headers possible in cors
    next();
})

app.use('/employees', employeesApp);
app.use('/admin', adminApp);

app.get("/",(req,res)=>
{
    res.send("Welcome Home");
})
// const mysql = require('mysql');
// app.use((req,res, next)=>{
//     res.write("ABC");
//     // res.end();
//     next();
// })

// app.use((req,res)=>{
//     res.write("XYZ");
//     res.end();
// })

// app.use(express.json()); //This is a middleware like app.use 
//                          //this will get us req.body in json format



app.listen(9999);